<?php

namespace Drupal\system_tags_breadcrumb\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface;
use Drupal\system_tags_breadcrumb\Entity\SystemTagBreadcrumb;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SystemTagForm.
 *
 * @package Drupal\system_tags_breadcrumb\Form
 */
class SystemTagBreadcrumbForm extends EntityForm {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The system tag finder manager.
   *
   * @var \Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface
   */
  protected $systemTagFinderManager;

  /**
   * SystemTagBreadcrumbForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface $systemTagFinderManager
   *   The system tag finder manager.
   */
  public function __construct(EntityTypeBundleInfoInterface $entityTypeBundleInfo, SystemTagFinderManagerInterface $systemTagFinderManager) {
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->systemTagFinderManager = $systemTagFinderManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.system_tags.system_tag_finder_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\system_tags_breadcrumb\Entity\SystemTagBreadcrumbInterface $entity */
    $entity = $this->getEntity();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Admin label'),
      '#description' => 'Provide an admin label for the System Tag breadcrumb.',
      '#default_value' => $entity->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => sprintf('%s::load', SystemTagBreadcrumb::class),
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $this->getEntityTypeOptions(),
      '#required' => TRUE,
      '#default_value' => $entity->get('entity_type'),
      '#ajax' => [
        'callback' => [$this, 'entityTypeUpdated'],
        'wrapper' => 'js-bundle-wrapper',
      ],
    ];

    $entity_type = $entity->get('entity_type') ?? $form_state->getValue('entity_type');
    $form['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => $this->getBundleOptions($entity_type),
      '#required' => TRUE,
      '#default_value' => $entity->get('bundle'),
      '#wrapper_attributes' => [
        'id' => 'js-bundle-wrapper',
      ],
    ];

    $form['system_tag'] = [
      '#type' => 'select',
      '#title' => $this->t('System Tag'),
      '#options' => $this->getSystemTagOptions(),
      '#required' => TRUE,
      '#default_value' => $entity->get('system_tag'),
    ];

    return $form;
  }

  /**
   * Build the entity type options.
   *
   * @return array
   *   An array containing entity type labels.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityTypeOptions(): array {
    $options = [];

    $plugin_definitions = $this->systemTagFinderManager->getDefinitions();
    foreach ($plugin_definitions as $plugin_definition) {
      $options[$plugin_definition['entity_type']] = $this->entityTypeManager->getDefinition($plugin_definition['entity_type'])
        ->getLabel();
    }

    $options = array_unique($options);
    natcasesort($options);

    return $options;
  }

  /**
   * Builds the bundle options for the given entity type.
   *
   * @param string|null $entity_type
   *   The selected entity type.
   *
   * @return array
   *   An array containing bundle labels.
   */
  protected function getBundleOptions(string $entity_type = NULL): array {
    $options = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);

    foreach ($bundles as $id => $bundle) {
      $options[$id] = $bundle['label'];
    }
    return $options;
  }

  /**
   * Builds the system tag options.
   *
   * @return array
   *   An array containing system tag labels;
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSystemTagOptions(): array {
    $options = [];

    $system_tags = $this->entityTypeManager->getStorage('system_tag')
      ->loadMultiple();

    foreach ($system_tags as $system_tag) {
      $options[$system_tag->id()] = $system_tag->label();
    }

    natcasesort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);

    if ($status === SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('Created new System Tag Breadcrumb %label.', [
          '%label' => $this->entity->label(),
        ]));
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Updated System Tag Breadcrumb %label.', [
          '%label' => $this->entity->label(),
        ]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $status;
  }

  /**
   * Ajax action when the entity type is updated.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The bundle form element.
   */
  public function entityTypeUpdated(array &$form, FormStateInterface $form_state): array {
    return $form['bundle'];
  }

}
