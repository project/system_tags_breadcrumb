<?php

namespace Drupal\system_tags_breadcrumb\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\ParamConverter\ParamConverterManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\system_tags\SystemTagFinder\SystemTagFinderInterface;
use Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface;
use Drupal\system_tags_breadcrumb\Entity\SystemTagBreadcrumbInterface;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SystemTagBasedBreadcrumbBuilder.
 *
 * @package Drupal\itr_job\Breadcrumb
 */
class SystemTagBasedBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * The breadcrumb builder.
   *
   * @var \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
   */
  protected $breadcrumbBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The param converter manager.
   *
   * @var \Drupal\Core\ParamConverter\ParamConverterManagerInterface
   */
  protected $paramConverterManager;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The sytem tag breadcrumb storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $systemTagBreadcrumbStorage;


  /**
   * The system tag finder manager.
   *
   * @var \Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface
   */
  protected $systemTagFinderManager;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * SystemTagBasedBreadcrumbBuilder constructor.
   *
   * @param \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface $breadcrumbBuilder
   *   The breadcrumb builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\ParamConverter\ParamConverterManagerInterface $paramConverterManager
   *   The param converter manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routeProvider
   *   The route provider.
   * @param \Drupal\system_tags\SystemTagFinder\SystemTagFinderManagerInterface $systemTagFinderManager
   *   The system tag finder manager.
   * @param \Drupal\Core\Controller\TitleResolverInterface $titleResolver
   *   The title resolver.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(BreadcrumbBuilderInterface $breadcrumbBuilder, EntityTypeManagerInterface $entityTypeManager, ParamConverterManagerInterface $paramConverterManager, RequestStack $requestStack, RouteProviderInterface $routeProvider, SystemTagFinderManagerInterface $systemTagFinderManager, TitleResolverInterface $titleResolver) {
    $this->breadcrumbBuilder = $breadcrumbBuilder;
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->paramConverterManager = $paramConverterManager;
    $this->routeProvider = $routeProvider;
    $this->systemTagBreadcrumbStorage = $entityTypeManager->getStorage('system_tag_breadcrumb');
    $this->systemTagFinderManager = $systemTagFinderManager;
    $this->titleResolver = $titleResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $applies = FALSE;

    $system_tag_breadcrumb = $this->getActiveSystemTagBreadcrumbEntity($route_match);
    if ($system_tag_breadcrumb instanceof SystemTagBreadcrumbInterface) {
      $applies = TRUE;
    }

    return $applies;
  }

  /**
   * Get the current active system tag breadcrumb entity.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface.
   *
   * @return \Drupal\system_tags_breadcrumb\Entity\SystemTagBreadcrumbInterface|null
   *   Returns a system tag breadcrumb entity if found, else NULL.
   */
  protected function getActiveSystemTagBreadcrumbEntity(RouteMatchInterface $route_match) {
    /** @var \Drupal\system_tags_breadcrumb\Entity\SystemTagBreadcrumbInterface[] $system_tag_breadcrumbs */
    $system_tag_breadcrumbs = $this->systemTagBreadcrumbStorage->loadMultiple();
    foreach ($system_tag_breadcrumbs as $system_tag_breadcrumb) {
      /** @var \Drupal\Core\Entity\EntityInterface $page_entity */
      $page_entity = $route_match->getParameter($system_tag_breadcrumb->get('entity_type'));
      if (!$page_entity instanceof EntityInterface) {
        continue;
      }

      if ($route_match->getRouteName() !== 'entity.' . $system_tag_breadcrumb->get('entity_type') . '.canonical') {
        continue;
      }

      if ((string) $system_tag_breadcrumb->get('bundle') !== (string) $page_entity->bundle()) {
        continue;
      }

      $system_tag_finder = $this->systemTagFinderManager->getInstance(['entity_type' => $system_tag_breadcrumb->get('entity_type')]);
      if (!$system_tag_finder instanceof SystemTagFinderInterface) {
        continue;
      }

      if ($system_tag_finder->findOneByTag($system_tag_breadcrumb->get('system_tag')) === NULL) {
        continue;
      }

      return $system_tag_breadcrumb;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['url.path']);
    $systemTagBreadcrumb = $this->getActiveSystemTagBreadcrumbEntity($route_match);

    if (!$systemTagBreadcrumb instanceof SystemTagBreadcrumbInterface) {
      return $breadcrumb;
    }

    $systemTagFinder = $this->systemTagFinderManager->getInstance(['entity_type' => $systemTagBreadcrumb->get('entity_type')]);
    if (!$systemTagFinder instanceof SystemTagFinderInterface) {
      return $breadcrumb;
    }

    $parent_page = $systemTagFinder->findOneByTag($systemTagBreadcrumb->get('system_tag'));

    if (!$parent_page instanceof EntityInterface) {
      return $breadcrumb;
    }

    $breadcrumb->addLink(Link::createFromRoute(new TranslatableMarkup('Home'), '<front>'));
    $breadcrumb->addLink(Link::fromTextAndUrl((string) $parent_page->label(), $parent_page->toUrl()));

    if ($this->currentRequest instanceof Request) {
      $title = $this->titleResolver->getTitle($this->currentRequest, $this->currentRequest->get(RouteObjectInterface::ROUTE_OBJECT));
      $url = Url::fromRouteMatch($route_match);
      $breadcrumb->addLink(new Link($title, $url));
    }

    return $breadcrumb;
  }

}
