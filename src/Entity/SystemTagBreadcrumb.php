<?php

namespace Drupal\system_tags_breadcrumb\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Class SystemTagBreadcrumb.
 *
 * @package Drupal\system_tags_breadcrumb\Entity
 *
 * @ConfigEntityType(
 *   id = "system_tag_breadcrumb",
 *   label = @Translation("System Tag breadcrumb"),
 *   handlers = {
 *     "list_builder" = "Drupal\system_tags_breadcrumb\SystemTagBreadcrumbListBuilder",
 *     "form" = {
 *       "add" = "Drupal\system_tags_breadcrumb\Form\SystemTagBreadcrumbForm",
 *       "edit" = "Drupal\system_tags_breadcrumb\Form\SystemTagBreadcrumbForm",
 *       "delete" = "Drupal\system_tags_breadcrumb\Form\SystemTagBreadcrumbDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\system_tags_breadcrumb\SystemTagBreadcrumbHtmlRouteProvider"
 *     },
 *   },
 *   config_prefix = "system_tags_breadcrumb",
 *   admin_permission = "administer system tag breadcrumbs",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/system_tags/breadcrumbs/add",
 *     "edit-form" = "/admin/structure/system_tags/breadcrumbs/{system_tag_breadcrumb}",
 *     "delete-form" = "/admin/structure/system_tags/breadcrumbs/{system_tag_breadcrumb}/delete",
 *     "collection" = "/admin/structure/system_tags/breadcrumbs"
 *   }
 * )
 */
class SystemTagBreadcrumb extends ConfigEntityBase implements SystemTagBreadcrumbInterface {

  /**
   * The ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;


  protected $entity_type;

  protected $bundle;

  protected $system_tag;

  public function calculateDependencies() {
    parent::calculateDependencies();

    $system_tag = \Drupal::entityTypeManager()
      ->getStorage('system_tag')
      ->load($this->system_tag)
    ;

    $this->addDependency('module', 'system_tags');
    $this->addDependency('config', $system_tag->getConfigDependencyName());

    return $this->dependencies;
  }

}
