<?php

namespace Drupal\system_tags_breadcrumb\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface SystemTagInterface.
 *
 * @package Drupal\system_tags_breadcrumb\Entity
 */
interface SystemTagBreadcrumbInterface extends ConfigEntityInterface {

}
