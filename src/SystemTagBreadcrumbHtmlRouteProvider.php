<?php

namespace Drupal\system_tags_breadcrumb;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Class SystemTagBreadcrumbHtmlRouteProvider.
 *
 * @package Drupal\system_tags_breadcrumb\SystemTagListBuilder
 */
class SystemTagBreadcrumbHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);
    if ($route) {
      $route->setOption('_admin_route', TRUE);
    }

    return $route;
  }

}
